export function formatDate(dateString:string) {
    const date = new Date(dateString);
    const options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };

    return date.toLocaleString('fr-FR', options);
}

export function truncateText(text:string) {
    var length = 25;
    if (text.length > length) {
        text = text.substring(0, length)+'...';
    }
    return text;
}