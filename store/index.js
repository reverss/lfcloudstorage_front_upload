import { createStore } from 'redux';

// Définissez l'action type
const SET_USER_ID = 0
const RESET = 'RESET';
const REMOVE_USER_ID = 'REMOVE_USER_ID';

export const reset = () => ({
    type: RESET,
});

// Définissez l'action creator
export const setUserId = (userId) => ({
    type: SET_USER_ID,
    payload: userId,
});

export const removeUserId = () => ({
    type: REMOVE_USER_ID,
});

// Définissez le reducer initial
const initialState = {
    user_id: null
};

// Définissez le reducer
const reducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_USER_ID:
            return {
                ...state,
                user_id: action.payload,
            };

        case REMOVE_USER_ID:
            return {
                ...state,
                user_id: null, // Supprimer la valeur de user_id en la définissant à null
            };

        case RESET:
            return initialState; // Réinitialiser l'état à sa valeur initiale

        default:
            return state;
    }
};

// Créez le store Redux
const store = createStore(reducer);

export default store;