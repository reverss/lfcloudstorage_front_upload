import {Button, Input, Loading, Row, Spacer, Tooltip} from "@nextui-org/react";
import {Mail} from "@/components/Icones/Mail";
import {Password} from "@/components/Icones/Password";
import React, {useState, useEffect} from "react";
import {Iconly} from "react-iconly";
import {useRouter} from 'next/navigation';
import axios from "axios";
import {motion} from 'framer-motion';
import ErrorAlert from "@/components/Alert/Error/ErrorAlert";
import {wait} from "@hapi/hoek";

export default function FormRegister() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        mail: '',
        password: '',
        cpassword: ''
    })

    const [password, setPassword] = useState('');

    const [stateLoading, setStateLoading] = useState(false)

    const [stateFirstName, setStateFirstName] = useState(false)
    const [stateLastName, setStateLastName] = useState(false)
    const [stateMail, setStateMail] = useState(false)
    const [stateSamePassword, setStateSamePassword] = useState(false)

    const [valuePassword, setValuePassword] = useState(null)

    const [stateFormCorrect, setStateFormCorrect] = useState(false)

    const [statePasswordLength, setStatePasswordLength] = useState(false)

    const [statePasswordNotGood, setStatePasswordNotGood] = useState(false)
    const [statePasswordGood, setStatePasswordGood] = useState(false)
    const [statePasswordVeryGood, setStatePasswordVeryGood] = useState(false)


    const [stateErrorMessage, setStateErrorMessage] = useState(false)

    const { push } = useRouter();

    const handleInputFirstNameChange = (event) => {
        setStateErrorMessage(false)
        setStateFirstName(true)

        if(event.target.value.length === 0) (
            setStateFirstName(false)
        )

        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleInputLastNameChange = (event) => {
        setStateErrorMessage(false)
        setStateLastName(true)

        if(event.target.value.length === 0) (
            setStateLastName(false)
        )

        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleInputMailNameChange = (event) => {
        setStateErrorMessage(false)
        setStateMail(true)

        if(event.target.value.length === 0) (
            setStateMail(false)
        )

        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleInputPasswordChange = (event) => {
        setStateErrorMessage(false)

        setValuePassword(event.target.value)


        const newPassword = event.target.value;
        setPassword(newPassword);

        setStatePasswordLength(true)

        const containsSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(newPassword);
        const containsUppercase = /[A-Z]/.test(newPassword);
        const containsLowercase = /[a-z]/.test(newPassword);

        if(event.target.value.length === 0)
        {
            setStatePasswordLength(false)
            setStatePasswordNotGood(false)
            setStatePasswordGood(false)
            setStatePasswordVeryGood(false)
        }
        else if (newPassword.length > 12 && containsUppercase && containsSpecialChar) {
            setStatePasswordVeryGood(true)
            setStatePasswordGood(true)
            console.log('Le mot de passe doit comporter au moins 12 caractères.');
        } else if (newPassword.length > 6 && containsUppercase && !containsSpecialChar ) {
            setStatePasswordGood(true)
            setStatePasswordNotGood(true)
            console.log('Le mot de passe doit contenir au moins un caractère spécial.');
        } else if (newPassword.length < 6 && containsLowercase && !containsUppercase && !containsSpecialChar || containsSpecialChar || containsUppercase || containsLowercase ) {
            setStatePasswordNotGood(true)
            console.log('Le mot de passe doit contenir au moins une lettre majuscule.');
        } else {
            console.log('Mot de passe valide !');
        }

        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleInputCPasswordChange = (event) => {
        if(valuePassword == event.target.value)
        {
            setStateSamePassword(true)
        }

        setStateErrorMessage(false)


        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {

        try {
            const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/register', formData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.status === 200) {

                const resultat = response.data['resultat'];
                const user_id = response.data['user']['id'];
                const token = response.data['user']['token'];
                let rolesObject = {};
                if (response.data['user']['roles']) {
                    rolesObject = response.data['user']['roles'];
                }


                if (resultat === 'success') {

                    console.log(rolesObject)

                    const rolesArray = Object.values(rolesObject);

                    rolesArray.forEach(role => {
                        if (typeof localStorage !== 'undefined') {
                            if (role === 'ROLE_NOTPURCHASE') {
                                localStorage.setItem('role_not_purchase', role)
                            } else if (role === 'ROLE_NOTVERIF') {
                                localStorage.setItem('role_not_verif', role)
                            }
                        }
                    })

                    if (typeof localStorage !== 'undefined') {
                        localStorage.setItem('token', token);
                        localStorage.setItem('user_id', user_id)
                        localStorage.setItem('user', JSON.stringify(response.data['user']))
                    }

                    push('/payment/stripe/plans')
                }

                console.log('Réponse JSON reçue depuis Symfony :', response.data);
            }
        } catch (error) {
            setStateErrorMessage(true)
            setStateLoading(false)
        }
    }

    useEffect(() => {

        console.log(stateErrorMessage)
        const setState = async() => {
            if (stateErrorMessage) {
                await wait(10000)
                await setStateErrorMessage(false)
            }
        }

        setState()
    }, [stateErrorMessage])

    /*if(stateLastName && stateFirstName && stateMail) {
    }*/

    return(
        <>
            {stateErrorMessage ? (
                <ErrorAlert>L'email est déjà utilisé, veuillez réessayer</ErrorAlert>
            ) : (
                <></>
            )}
            <form className={'space-y-6'} onSubmit={handleSubmit} method={"POST"}>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1}}
                    transition={{
                        type: "spring",
                        duration: 2,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                    className={''}
                >
                    <Row className={'gap-5'} css={{ alignItems: "center" }} justify="space-between">
                        <Input
                            value={formData.first_name}
                            onChange={handleInputFirstNameChange}
                            name={"first_name"}
                            aria-label={"First name"}
                            clearable
                            bordered
                            color="primary"
                            size="lg"
                            placeholder="Prénom"
                            required={true}
                            contentLeft={<Iconly name='Edit' set='bold' size='xlarge' />}
                        />
                        <Input
                            value={formData.last_name}
                            onChange={handleInputLastNameChange}
                            name={"last_name"}
                            aria-label={"Last name"}
                            clearable
                            bordered
                            color="primary"
                            size="lg"
                            placeholder="Nom de famille"
                            required={true}
                            contentLeft={<Iconly name='Edit' set='bold' size='xlarge' />}
                        />
                    </Row>
                </motion.div>

                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.2,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                    className={''}
                >
                    <Input
                        value={formData.mail}
                        onChange={handleInputMailNameChange}
                        name={"mail"}
                        aria-label={"Mail"}
                        fullWidth
                        clearable
                        bordered
                        color="error"
                        size="lg"
                        placeholder="Adresse-mail"
                        required={true}
                        //@ts-ignore
                        contentLeft={<Mail fill="currentColor"/>}
                    />
                </motion.div>

                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1}}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.4,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                    className={''}
                >
                    <Input.Password
                        value={formData.password}
                        onChange={handleInputPasswordChange}
                        name={"password"}
                        aria-label={"Password"}
                        fullWidth
                        clearable
                        bordered
                        color="error"
                        size="lg"
                        placeholder="Mot de passe"
                        type={'password'}
                        required={true}
                        //@ts-ignore
                        contentLeft={<Password fill="currentColor"></Password>}
                    />
                    {!statePasswordLength ? (
                        <></>
                    ) : (
                        <motion.div
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            transition={{
                                type: "spring",
                                duration: 2,
                                ease: [0, 0.71, 0.2, 1.01]
                            }}
                            className={'flex w-3/6 h-1.5 bg-gray-800/10 rounded-xl ml-2 mt-3 -mb-1'}
                        >
                            {statePasswordNotGood ? (
                                <div className={'w-[33.3%] h-full bg-red-500 rounded-l-xl'}></div>
                            ) : (
                                <div className={'hidden w-[33.3%] h-full bg-red-500 rounded-l-xl'}></div>
                            )}

                            {statePasswordGood ? (
                                <div className={'w-[33.3%] h-full bg-yellow-500'}></div>
                            ) : (
                                <div className={'hidden w-[33.3%] h-full bg-yellow-500'}></div>
                            )}

                            {statePasswordVeryGood ? (
                                <div className={'w-[33.3%] h-full bg-green-500 rounded-r-xl'}></div>
                            ) : (
                                <div className={'hidden w-[33.3%] h-full bg-green-500 rounded-r-xl'}></div>
                            )}
                        </motion.div>
                    )}
                </motion.div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.6,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                    className={''}
                >
                    <Input
                        value={formData.cpassword}
                        onChange={handleInputCPasswordChange}
                        name={"cpassword"}
                        aria-label={"Confirmate Password"}
                        fullWidth
                        clearable
                        bordered
                        color="error"
                        size="lg"
                        placeholder="Confirmer votre mot de passe"
                        type={'password'}
                        required={true}
                        //@ts-ignore
                        contentLeft={<Password fill="currentColor"/>}
                    />
                </motion.div>
                <Spacer y={1} />
                {/*{stateFirstName && stateLastName && stateMail && stateSamePassword && !stateLoading ? (
                    <Button onPress={() => {setStateLoading(true)}} type="submit" css={{ width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', borderRadius: '10px', height: '3rem', '&:hover': { background: '$purple700'} }} className={"uppercase tracking-wider mt-10 hover:-translate-y-1"}>
                        Inscription
                    </Button>
                ) : (
                    <Button type="submit" disabled css={{pointerEvents: 'unset !important', cursor: 'not-allowed !important', width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', border: '2px solid transparent', borderRadius: '10px', height: '3rem'}} className={"uppercase tracking-wider mt-10"}>
                        Inscription
                    </Button>
                )}*/}

                {stateLoading ? (
                    <Button type="submit" css={{ width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', border: '2px solid transparent', borderRadius: '7px', height: '3rem' }} className={"mt-10 hover:-translate-y-1"}>
                        <Loading color="currentColor" size="sm" />
                    </Button>
                ) : (
                    <Button onPress={() => {setStateLoading(true)}} type="submit" css={{ width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', borderRadius: '10px', height: '3rem', '&:hover': { background: '$purple700'} }} className={"uppercase tracking-wider mt-10 hover:-translate-y-1"}>
                        Inscription
                    </Button>
                )}
            </form>
        </>
    )
}