import {Button, Input, Link, Spacer, Text, User} from "@nextui-org/react";
import {Mail} from "@/components/Icones/Mail";
import {Password} from "@/components/Icones/Password";
import React, {useEffect, useMemo, useState} from "react";
import {usePathname, useRouter} from 'next/navigation';
import {motion} from "framer-motion";
import axios from "axios";
import UploadProfilePictureModal from "@/components/Modal/UploadProfilePictureModal";
import {Label} from "reactstrap";

export default function FormProfile() {
    const [user, setUser] = useState({})
    const [userId, setUserId] = useState()

    useEffect(()=> {
        const localUser = localStorage.getItem('user')
        const localUserId = localStorage.getItem('user_id')

        if(localUser) {
            setUser(JSON.parse(localUser))
            setUserId(localUserId)
        }

    }, [])


    const [formData, setFormData] = useState({
        phone_number: '',
        postal_code: '',
        adress: '',
        user_id: ''

    })

    formData.user_id = userId

    // @ts-ignore
    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    // @ts-ignore
    const handleSubmit = async (event) => {

        event.preventDefault();

        console.log(formData)

        try {
            const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/my-profile/update-profile', formData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.status === 200) {
                if(response.data['resultat'] == 'success')
                {
                    if (typeof localStorage !== 'undefined') {
                        localStorage.setItem('user', JSON.stringify(response.data['user']))
                    }

                    // Envoi de message
                }
                console.log('Réponse JSON reçue depuis Symfony :', response.data);
            } else {
                console.error('Erreur lors de la soumission du formulaire à Symfony');
            }
        } catch (error) {
            console.error('Erreur lors de la soumission du formulaire à Symfony', error);
        }
    }

    // @ts-ignore
    return(

            <form onSubmit={handleSubmit} method={"POST"}>
                <section className={"h-[600px] overflow-auto"}>
                    <motion.div
                        className={"flex w-3/5 h-[10px] mx-auto mt-14"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 0.6,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <div className={'relative -mt-6'}>
                            <UploadProfilePictureModal/>
                            {user.picture != "" ? (
                                <User src={user.user_picture} className={"profile_picture"} name={" "} size={'xl'}/>
                            ) : (
                                <User className={"profile_picture"} name={" "} size={'xl'}/>
                            )}
                        </div>
                        <div className={"flex-col -mt-6 space-y-3"}>
                            <Text color={'white'} className={"text-lg tracking-wide"}>Avatar</Text>
                            <Text color={'white'} className={"text-xs opacity-50 tracking-wider"}>Min 200x200px | .PNG ou .JPG/JPEG</Text>
                        </div>
                    </motion.div>
                    <motion.div
                        className={"flex flex-col gap-y-2 w-3/5 mx-auto mt-16"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 0.8,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Label className={"text-white text-sm tracking-wider opacity-60"}>Prénom</Label>
                        <input className={"-ml-1 p-2.5 rounded-xl bg-gray-100/5 shadow-md cursor-not-allowed"} disabled placeholder={user.first_name}/>
                    </motion.div>
                    <motion.div
                        className={"flex flex-col gap-y-2 w-3/5 mx-auto mt-5"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 1,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Label className={"text-white text-sm tracking-wider opacity-60"}>Nom de famille</Label>
                        <input className={"-ml-1 p-2.5 rounded-xl bg-gray-100/5 shadow-md cursor-not-allowed"} disabled placeholder={user.last_name}/>
                    </motion.div>
                    <motion.div
                        className={"flex items-center w-3/5 mx-auto mt-5 space-x-4 justify-between"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 1.2,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <div className={'flex-col space-y-2'}>
                            <Label className={"text-white text-sm tracking-wider opacity-60"}>Téléphone</Label>
                            {user.phone_number !== null ? (
                                <Input
                                    value={formData.phone_number}
                                    onChange={handleInputChange}
                                    name={"phone_number"}
                                    aria-label={"Phone_number"}
                                    clearable
                                    bordered
                                    fullWidth
                                    color="primary"
                                    size="lg"
                                    placeholder={user.phone_number}
                                    shadow={false}
                                    css={{ boxShadow: 'none', marginLeft: '-0.25rem !important'}}
                                />
                            ) : (
                                <Input
                                    value={formData.phone_number}
                                    onChange={handleInputChange}
                                    name={"phone_number"}
                                    aria-label={"Phone_number"}
                                    clearable
                                    bordered
                                    fullWidth
                                    color="primary"
                                    size="lg"
                                    shadow={false}
                                    css={{ boxShadow: 'none', marginLeft: '-0.25rem !important' }}
                                />
                            )}
                        </div>
                        <div className={"flex-col space-y-2"}>
                            <Label className={"text-white text-sm tracking-wider opacity-60"}>Code postal</Label>
                            {user.postal_code !== null ? (
                                <Input
                                    value={formData.postal_code}
                                    onChange={handleInputChange}
                                    name={"postal_code"}
                                    aria-label={"Postal_code"}
                                    clearable
                                    bordered
                                    fullWidth
                                    color="primary"
                                    size="lg"
                                    placeholder={user.postal_code}
                                    shadow={false}
                                    css={{ boxShadow: 'none', marginLeft: '-0.25rem !important' }}
                                />
                            ) : (
                                <Input
                                    value={formData.postal_code}
                                    onChange={handleInputChange}
                                    name={"postal_code"}
                                    aria-label={"Postal_code"}
                                    clearable
                                    bordered
                                    fullWidth
                                    color="primary"
                                    size="lg"
                                    shadow={false}
                                    css={{ boxShadow: 'none', marginLeft: '-0.25rem !important' }}
                                />
                            )}
                        </div>
                    </motion.div>
                    <motion.div
                        className={"flex flex-col gap-y-2 w-3/5 mx-auto mt-5"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 1.4,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Label className={"text-white text-sm tracking-wider opacity-60"}>Adresse</Label>
                        {user.adress !== null ? (
                            <Input
                                value={formData.adress}
                                onChange={handleInputChange}
                                name={"adress"}
                                aria-label={"Adress"}
                                clearable
                                bordered
                                fullWidth
                                color="primary"
                                size="lg"
                                placeholder={user.adress}
                                shadow={false}
                                css={{ boxShadow: 'none', marginLeft: '-0.25rem !important' }}
                            />
                        ) : (
                            <Input
                                value={formData.adress}
                                onChange={handleInputChange}
                                name={"address"}
                                aria-label={"Address"}
                                clearable
                                bordered
                                fullWidth
                                color="primary"
                                size="lg"
                                shadow={false}
                                css={{ boxShadow: 'none', marginLeft: '-0.25rem !important' }}
                            />
                        )}
                    </motion.div>
                    <motion.div
                        className={"flex flex-col gap-y-2 w-3/5 mx-auto mt-5"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 1.6,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Button css={{marginTop: '5px', height: '45px'}} className={"tracking-wider"} color={'success'} type={'submit'}>Sauvegarder</Button>
                    </motion.div>
                </section>
            </form>
    )
}
