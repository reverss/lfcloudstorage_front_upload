import {Button, Input, Link, Loading, Spacer} from "@nextui-org/react";
import {Mail} from "@/components/Icones/Mail";
import {Password} from "@/components/Icones/Password";
import React, {useEffect, useMemo, useState} from "react";
import {useRouter} from 'next/navigation';
import {motion} from "framer-motion";
import axios from "axios";
import ErrorAlert from "@/components/Alert/Error/ErrorAlert";
import {wait} from "@hapi/hoek";

export default function FormConnexion() {
    const [formData, setFormData] = useState({
        mail: '',
        password: ''
    })

    const [stateLoading, setStateLoading] = useState(false)
    const [stateErrorMessage, setStateErrorMessage] = useState(false)

    const { push } = useRouter();

    // @ts-ignore
    const handleInputChange = (event) => {
        setStateErrorMessage(false)

        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
    };

    // @ts-ignore
    const handleSubmit = async (event) => {

        event.preventDefault();

        try {
            const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/login', formData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.status === 200) {
                const resultat = response.data['resultat'];
                const user_id = response.data['user']['id'];
                const token = response.data['user']['token'];

                let rolesObject = {};
                if (response.data['user']['roles']) {
                    rolesObject = response.data['user']['roles'];
                }

                if (resultat === 'success') {

                    const rolesArray = Object.values(rolesObject);

                    rolesArray.forEach(role => {
                        if (typeof localStorage !== 'undefined') {
                            if (role === 'ROLE_NOTPURCHASE') {
                                localStorage.setItem('role_not_purchase', role)
                            } else if (role === 'ROLE_NOTVERIF') {
                                localStorage.setItem('role_not_verif', role)
                            }
                        }
                    })

                    if (typeof localStorage !== 'undefined') {
                        localStorage.setItem('token', token);
                        localStorage.setItem('user_id', user_id)
                        localStorage.setItem('user', JSON.stringify(response.data['user']))
                    }

                    push('/');
                }
                console.log('Réponse JSON reçue depuis Symfony :', response.data);
            }
        } catch (error) {
            setStateErrorMessage(true)
            setStateLoading(false)
        }
    }


    useEffect(() => {

        console.log(stateErrorMessage)
        const setState = async() => {
            if (stateErrorMessage) {
                await wait(10000)
                await setStateErrorMessage(false)
            }
        }

        setState()
    }, [stateErrorMessage])

    return(
        <>
            {stateErrorMessage ? (
                <ErrorAlert>Identifiants incorrect, veuillez réessayer.</ErrorAlert>
            ) : (
                <></>
            )}
            <form className={"w-5/6 mx-auto"} onSubmit={handleSubmit} action={"https://lfcloudstorageapi.reverss.fr/api/login"} method={"POST"}>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.3,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    <Input
                        value={formData.mail}
                        onChange={handleInputChange}
                        name={"mail"}
                        aria-label={"Mail"}
                        clearable
                        bordered
                        fullWidth
                        color="primary"
                        size="lg"
                        placeholder="Adresse-mail"
                        shadow={false}
                        //@ts-ignore
                        contentLeft={<Mail fill={"currentColor"}></Mail>}
                        css={{ boxShadow: 'none', background: 'white' }}
                    />
                </motion.div>
                <Spacer y={1} />
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.6,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    <Input.Password
                        css={{ inputBorderRadius: '1px', '&:hover': { borderColor: '$purple500'} }}
                        value={formData.password}
                        onChange={handleInputChange}
                        name={"password"}
                        aria-label={"Password"}
                        clearable
                        bordered
                        fullWidth
                        color="primary"
                        size="lg"
                        type={"password"}
                        placeholder="Mot de passe"
                        shadow={false}
                        //@ts-ignore
                        contentLeft={<Password fill="currentColor" />}
                    />
                </motion.div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.9,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    {!stateLoading ? (
                        <Button onPress={() => {setStateLoading(true)}} type="submit" css={{ width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', border: '2px solid transparent', borderRadius: '7px', height: '3rem', '&:hover': { background: 'transparent', borderColor: '$purple600', color: '$white'} }} className={"buttonGlitch after:content-['CONNEXION'] mt-10 hover:-translate-y-1"}>
                            Connexion
                        </Button>
                    ) : (
                        <Button onPress={() => {setStateLoading(true)}} type="submit" css={{ width: '100%', background: 'linear-gradient(112deg, #B583E7 -63.59%, #571D91 -20.3%, #571D91 70.46%)', border: '2px solid transparent', borderRadius: '7px', height: '3rem' }} className={"mt-10 hover:-translate-y-1"}>
                            <Loading color="currentColor" size="sm" />
                        </Button>
                    )}
                </motion.div>
            </form>
        </>
    )
}
