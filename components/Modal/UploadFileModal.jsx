import { Modal } from "@nextui-org/react";
import React, {useCallback, useEffect, useState} from "react";
import {motion} from "framer-motion";
import dynamic from "next/dynamic";
import {MoreIcon} from "@/components/Icones/Dashboard/MoreIcon";
import {ArrowUploadIcon} from "@/components/Icones/Dashboard/ArrowUploadIcon";
import Image from "next/image";
import {TrashIcon} from "@/components/Icones/Dashboard/TrashIcon";
import {useDropzone} from "react-dropzone";
import axios from "axios";
import {className} from "postcss-selector-parser";
import {useRouter} from "next/navigation";


function UploadFilesModal() {

    const [visible, setVisible] = React.useState(false);
    const handler = () => {
        setVisible(true)
        removeAll()
    }
    const closeHandler = () => {
        setVisible(false);
        console.log("closed");
    };

    const [files, setFiles] = useState([])
    const [rejected, setRejected] = useState([])

    const onDrop = useCallback((acceptedFiles, rejectedFiles) => {
        if (acceptedFiles?.length) {
            setFiles(() => [
                ...acceptedFiles.map(file =>
                    Object.assign(file, { preview: URL.createObjectURL(file) })
                )
            ])
        }

        if (rejectedFiles?.length) {
            setRejected(previousFiles => [...previousFiles, ...rejectedFiles])
        }
    }, [])

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        accept: {
            'image/*': []
        },
        maxSize: 2048 * 1080,
        maxFiles: 4,
        onDrop
    })

    useEffect(() => {
        // Revoke the data uris to avoid memory leaks
        return () => files.forEach(file => URL.revokeObjectURL(file.preview))

    }, [files])

    const removeFile = name => {
        setFiles(files => files.filter(file => file.name !== name))
    }

    const removeAll = () => {
        setFiles([])
        setRejected([])
    }

    const removeRejected = name => {
        setRejected(files => files.filter(({ file }) => file.name !== name))
    }

    const router = useRouter()

    const handleSubmit = async (event) => {

        event.preventDefault();

        const formData = new FormData();

        files.forEach((file) => {
            formData.append('files[]', file)
        })

        if (typeof localStorage !== 'undefined') {
            formData.append('user_id', localStorage.getItem('user_id'));
        }

        try {

            const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/my-storage-space/files/upload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            console.log(response.data)

            if (response.status === 200) {
                const resultat = response.data['resultat'];

                if (resultat === 'success') {

                    removeAll();
                    setVisible(false)
                    window.location.reload()
                }

                if (resultat === 'error_file_already_exist') {
                    event.preventDefault();
                    removeAll();
                    // Message d'erreur et reset
                }

                console.log('Réponse JSON reçu depuis Symfony :', response.data);
            } else {
                console.error('Erreur lors de la soumission du formulaire à Symfony');
            }
        } catch (error) {
            console.error('Une erreur est s\' est produite lors du téléchargement des fichiers.', error);
        }
    }

    return (
        <div className={"w-full mx-auto"}>
            <motion.button
                className={'flex items-center'}
                whileHover={{
                    scale : 1.05,
                    transition: {
                        duration: 0.2
                    }
                }}
                onClick={handler}
            >
                <span className={'flex justify-center items-center w-[35px] h-[35px] bg-white rounded-full'}><MoreIcon fill={'#7828C8'} width={35} height={35}/></span>
                <span className={'text-white ml-3 text-xs tracking-wider uppercase font-bold'}>Add file</span>
            </motion.button>
            <Modal
                width={"700px"}
                blur={true}
                closeButton
                aria-labelledby="modal-register"
                open={visible}
                onClose={closeHandler}
            >
                <Modal.Body className={"w-11/12 mx-auto"}>
                    <form className={'w-11/12 mx-auto mt-5'} method={'POST'} onSubmit={handleSubmit} encType="multipart/form-data">
                        <div
                            {...getRootProps({
                                className: className
                            })}
                        >
                            <input {...getInputProps({ name: 'file' })} type='file' name='file'/>
                            <div className='flex flex-col items-center justify-center gap-4 cursor-pointer border-2 p-8 rounded-xl text-xs font-medium'>
                                <ArrowUploadIcon/>
                                {isDragActive ? (
                                    <p>Dépose le fichier ici...</p>
                                ) : (
                                    <div className={"text-center"}>
                                        <p>Glissez / Déposez votre fichier ici</p>
                                        ou
                                        <p>Cliquez puis séléctionner vos fichiers à uploadés.</p>
                                    </div>
                                )}
                            </div>
                        </div>

                        {/* Preview */}
                        <section className='mt-10'>
                            <div className='flex gap-4'>
                                <h2 className='title text-2xl font-bold'>Prévisualisation</h2>
                                <button
                                    type='submit'
                                    className='ml-auto mt-1 rounded-md border border-purple-400 px-3 text-[12px] font-bold uppercase tracking-wider text-stone-500 transition-colors hover:bg-purple-400 hover:text-white'
                                >
                                    Upload
                                </button>
                            </div>

                            <button
                                type='button'
                                onClick={removeAll}
                                className='mt-6 rounded-md border border-rose-400 p-2 text-[11px] font-bold uppercase tracking-wider text-stone-500 transition-colors hover:bg-rose-400 hover:text-white'
                            >
                                Supprimer tous les fichiers
                            </button>

                            {/* Accepted files */}
                            <h3 className='title mt-6 border-b pb-3 text-lg font-semibold text-stone-600'>
                                Fichiers acceptés
                            </h3>
                            <ul className='mt-6 grid grid-cols-1 gap-10 gap-y-24 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4'>
                                {files.map(file => (
                                    <li key={file.name} className='relative h-28 rounded-md shadow-lg'>
                                        <Image
                                            src={file.preview}
                                            alt={file.name}
                                            width={100}
                                            height={100}
                                            onLoad={() => {
                                                URL.revokeObjectURL(file.preview)
                                            }}
                                            className='h-full w-full rounded-md object-contain'
                                        />
                                        <button
                                            type='button'
                                            className='absolute -right-3 -top-3 flex h-7 w-7 items-center justify-center rounded-full border border-rose-400 bg-rose-400 transition-colors hover:bg-white'
                                            onClick={() => removeFile(file.name)}
                                        >
                                            <TrashIcon/>
                                        </button>
                                        <p className='truncate mt-2 text-[11px] font-medium text-stone-500'>
                                            {file.name}
                                        </p>
                                    </li>
                                ))}
                            </ul>

                            {/* Rejected Files */}
                            <h3 className='title mt-16 border-b pb-3 text-lg font-semibold text-stone-600'>
                                Fichiers rejetés
                            </h3>
                            <ul className='mt-6 flex flex-col'>
                                {rejected.map(({ file, errors }) => (
                                    <li key={file.name} className='flex items-start justify-between'>
                                        <div>
                                            <p className='mt-2 text-sm font-medium text-stone-500'>
                                                {file.name}
                                            </p>
                                            <ul className='text-[12px] text-red-400'>
                                                {errors.map(error => (
                                                    <li key={error.code}>{error.message}</li>
                                                ))}
                                            </ul>
                                        </div>
                                        <button
                                            type='button'
                                            className='mt-1 rounded-md border border-rose-400 px-3 py-1 text-[12px] font-bold uppercase tracking-wider text-stone-500 transition-colors hover:bg-rose-400 hover:text-white'
                                            onClick={() => removeRejected(file.name)}
                                        >
                                            supprimer
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </section>
                    </form>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default dynamic(()=>Promise.resolve(UploadFilesModal), {ssr: false})