import { Modal, Button, Text } from "@nextui-org/react";
import FormRegister from "@/components/Form/FormRegister";
import React from "react";
import {motion} from "framer-motion";

export default function RegisterModal({ buttonText = 'Créer son compte' }) {
    const [visible, setVisible] = React.useState(false);
    const handler = () => setVisible(true);
    const closeHandler = () => {
        setVisible(false);
        console.log("closed");
    };

    return (
        <div className={"w-[83%] mx-auto"}>
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{
                    type: "spring",
                    duration: 2,
                    delay: 1.3,
                    ease: [0, 0.71, 0.2, 1.01]
                }}
            >
                <Button onPress={handler} css={{ width: '100%', background: 'transparent', border: '1px solid $white', borderRadius: '7px', height: '3rem' }} className={"buttonGlitch after:content-['créer_son_compte'] mt-5 hover:-translate-y-1"}>créer son compte</Button>
            </motion.div>
            <Modal
                width={"700px"}
                blur={true}
                closeButton
                aria-labelledby="modal-register"
                open={visible}
                onClose={closeHandler}
                css={{padding: '20px'}}
            >
                <Modal.Header>
                    <Text id="modal-title" className={"tracking-wider font-medium"} size={25}>
                        Inscription
                    </Text>
                </Modal.Header>
                <Modal.Body className={"w-11/12 mx-auto"}>
                    <FormRegister></FormRegister>
                </Modal.Body>
            </Modal>
        </div>
    );
}