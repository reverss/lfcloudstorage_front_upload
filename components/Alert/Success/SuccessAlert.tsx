import {motion} from "framer-motion";
import {Card, Text, User} from "@nextui-org/react";

export default function SuccessAlert({children, className}: {
    children: React.ReactNode
    className: string
}) {

    return(
        <>
            <div className={'w-full'}>
                <motion.div
                    initial={{ opacity: 0, y: -100 }}
                    animate={{ opacity: 1, y: 0 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.3,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    <Card isHoverable variant="bordered" css={{ mw: "400px" }}>
                        <Card.Body>
                            <Text>Success Message.</Text>
                        </Card.Body>
                    </Card>
                </motion.div>
            </div>
        </>
    )
}