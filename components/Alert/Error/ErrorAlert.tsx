import {motion} from "framer-motion";
import {Card, Text, User} from "@nextui-org/react";

export default function ErrorAlert({children}: {
    children: React.ReactNode
}) {
    return(
        <>
            <div className={'flex justify-center w-full ml-4 -mt-6 mb-4'}>
                <motion.div
                    initial={{ opacity: 0, x: -100 }}
                    animate={{ opacity: 1, x: -10 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.3,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                    className={'w-fit'}
                >
                    <Card isHoverable variant="bordered" css={{ width: '100%', background: "#F31260", marginBottom: '15px'}}>
                        <Card.Body css={{fontWeight: '500', padding: '15px 20px 15px 20px !important'}}>
                            <Text className={'px-2 tracking-wide text-sm text-center'} css={{fontWeight: '500'}} color={"white"}>{children}</Text>
                        </Card.Body>
                    </Card>
                </motion.div>
            </div>
        </>


    )
}