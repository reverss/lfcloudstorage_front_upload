import { useState, useEffect } from 'react';
import {Link} from "@nextui-org/react";
import {usePathname, useRouter} from "next/navigation";

const LinkProfileComponent = ({ href, children, className }) => {
    const pathname = usePathname();
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        // Vérifier si l'URL correspond au lien
        if (pathname === href) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [pathname, href]);


    return (
        <a className={isActive ? 'text-[#7828C8] bg-[#F7ECFC]' : className } href={href}>
            {children}
        </a>
    );
};

export default LinkProfileComponent;