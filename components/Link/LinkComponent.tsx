import { useState, useEffect } from 'react';
import {Link} from "@nextui-org/react";
import {usePathname, useRouter} from "next/navigation";

const LinkComponent = ({ href, children, className }) => {
    const pathname = usePathname();
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        // Vérifier si l'URL correspond au lien
        if (pathname === href) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [pathname, href]);


    return (
        <a className={isActive ? 'activeLink hover:text-white' + ' ' + className : className } href={href}>
            {children}
        </a>
    );
};

export default LinkComponent;