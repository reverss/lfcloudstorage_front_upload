import {Link} from "@nextui-org/react";


export  default function ProfileNavbarLeft() {

    return(
        <>
            <nav className="w-[19%] h-full border-r-[1.5px] font-bold text-[12px] uppercase m-6">
                <ul className={"text-zinc-600/50 pt-5 space-y-3"}>
                    <li>
                        <a className={"hover:text-zinc-800 duration-300"} href={""}>Profil</a>
                    </li>
                    <li>
                        <a className={"hover:text-zinc-800 duration-300"} href={""}>Sécurité</a>
                    </li>
                    <li></li>
                </ul>
                <ul className={"mt-10"}>
                    <li>
                        <a className={"text-red-500 hover:text-red-700 duration-300"} href={""}>Supprimer mon compte</a>
                    </li>
                </ul>
            </nav>
        </>
    )
}