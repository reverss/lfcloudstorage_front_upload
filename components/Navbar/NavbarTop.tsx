import {motion} from "framer-motion";
import {StorageIcon} from "@/components/Icones/Dashboard/StorageIcon";
import {User} from "@nextui-org/react";

export default function NavbarTop({children, className}: {
    children: React.ReactNode
    className: string
}) {

    return(
        <>
            <section className={className + " " + "flex items-center w-full h-24 shadow-lg shadow-white/5"}>
                {children}
                <motion.div
                    className={"flex items-center"}
                    initial={{ opacity: 0, x: 100 }}
                    animate={{ opacity: 1, x: -20 }}
                    transition={{
                        type: "spring",
                        duration: 2,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    <div className={'flex items-center p-4 gap-4 w-auto h-12 border border-white rounded-2xl font-medium text-xs uppercase tracking-wider'}>
                        <span className={'mb-[2px]'}><StorageIcon fill={'#9750DD'} width={20} height={20}/></span>
                        <span className={'text-[#9750DD]'}>10 / 20 Gb</span>
                        <span className={'text-white'}>Stockage utilisé</span>
                    </div>
                    <a href={'/dashboard/profile'}>
                        <User
                            css={{border: 'none'}}
                            bordered
                            size={"lg"}
                            name={" "}
                            src="https://i.pravatar.cc/150?u=a042581f4e29026704d"
                            pointer
                        />
                    </a>
                </motion.div>
            </section>
        </>
    )
}