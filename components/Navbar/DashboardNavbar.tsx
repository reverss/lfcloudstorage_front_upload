import Image from "next/image";
import {Link} from "@nextui-org/react";
import LinkComponent from "@/components/Link/LinkComponent";
import {DashboardIcon} from "@/components/Icones/Dashboard/DashboardIcon";
import {ProfilIcon} from "@/components/Icones/Dashboard/ProfilIcon";
import {SettingsIcon} from "@/components/Icones/Dashboard/SettingsIcon";
import {FilesIcon} from "@/components/Icones/Dashboard/FilesIcon";
import {LogoutIcon} from "@/components/Icones/Dashboard/LogoutIcon";
import {TrashIcon} from "@/components/Icones/Dashboard/TrashIcon";
import {HelpIcon} from "@/components/Icones/Dashboard/HelpIcon";

export  default function DashboardNavbar() {

    return(
        <>
            <nav className="fixed w-[19%] h-screen left-0 block top-0 bottom-0 overflow-y-auto flex-row flex-nowrap overflow-hidden bg-[#191a1c] shadow-lg shadow-white/5 items-center justify-between z-10 py-4 px-6">
                <div className="relative h-full flex-nowrap px-0 flex justify-between w-full mx-auto">
                    <div className="w-full">
                        <div className="block">
                            <div className="flex flex-wrap justify-center mr-4">
                                <Link href={'/dashboard'}>
                                    <div className="glitch">
                                        <Image
                                            width={100}
                                            height={100}
                                            src="/images/iconedd.png"
                                            alt=""/>
                                        <div className="glitch__layers">
                                            <div className="glitch__layer"></div>
                                            <div className="glitch__layer"></div>
                                            <div className="glitch__layer"></div>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <h6 className="md:min-w-full text-white uppercase text-xs tracking-[1px] font-semibold block mt-5 pt-1 pb-4 no-underline">
                            Mon espace
                        </h6>

                        <ul className="flex flex-col list-none space-y-2">
                            <li className="items-center">
                                <LinkComponent className={'group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300'} href={'/dashboard'}>
                                    <DashboardIcon/>
                                    Dashboard
                                </LinkComponent>
                            </li>
                            <li className="items-center">
                                <LinkComponent className={'group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300'} href={'/dashboard/profile'}>
                                    <ProfilIcon/>
                                    Mon profil
                                </LinkComponent>
                            </li>

                            {/*<li className="items-center">
                                <LinkComponent className={'group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300'} href={'/dashboard/settings'}>
                                    <SettingsIcon/>
                                    Réglages
                                </LinkComponent>
                            </li>*/}
                        </ul>
                        <hr className="mt-8 mb-10 w-full"/>
                        <h6 className="md:min-w-full text-white text-xs tracking-[1px] uppercase font-semibold block pt-1 pb-4 no-underline">
                            MON ESPACE DE STOCKAGE
                        </h6>

                        <ul className="flex flex-col list-none space-y-2">
                            <li className="items-center">
                                <LinkComponent className="group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300" href={'/dashboard/files/show'}>
                                    <FilesIcon/>
                                    Mes fichiers
                                </LinkComponent>
                            </li>
                            {/*<li className="items-center">
                                <LinkComponent className="group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300" href={'/dashboard/files/trash'}>
                                    <TrashIcon width={20} height={20}/>
                                    Corbeille
                                </LinkComponent>
                            </li>*/}
                        </ul>
                        <ul className={'fixed bottom-5'}>
                            <li className="items-center">
                                <LinkComponent className="group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300" href={'/security/logout'}>
                                    <HelpIcon/>
                                    Aide / Support
                                </LinkComponent>
                            </li>
                            <li className="items-center">
                                <LinkComponent className="group flex gap-2 text-xs items-center uppercase font-semibold text-gray-300 py-3 hover:text-gray-500 duration-300" href={'/security/logout'}>
                                    <LogoutIcon/>
                                    Déconnexion
                                </LinkComponent>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}