import {Table, Tooltip, User} from "@nextui-org/react";
import {UtilsIcon} from "@/components/Icones/Table/UtilsIcon";
import {IconButton} from "@/components/Icones/IconButton";
import {DeleteIcon} from "@/components/Icones/Table/DeleteIcon";

export const SkeletonCard = () => {
    return (
        <>
            <Table className={'w-4/5 bg-white shadow-md'}
                   aria-label="Example disabled keys collection table"
                   css={{
                       height: "auto",
                       minWidth: "100%",
                   }}
                   selectionMode="multiple"
                   color="secondary"
            >
                <Table.Header>
                    <Table.Column>LOGO</Table.Column>
                    <Table.Column>NOM DE FICHIER</Table.Column>
                    <Table.Column>TAILLE</Table.Column>
                    <Table.Column>DATE UPLOAD</Table.Column>
                    <Table.Column>EXTENSION</Table.Column>
                    <Table.Column><UtilsIcon stroke={'#7E868C'}/></Table.Column>
                </Table.Header>
                <Table.Body>
                    <Table.Row css={{fontSize: '12px', fontWeight: '700', letterSpacing: '0.5px'}}>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                    </Table.Row>
                    <Table.Row css={{fontSize: '12px', fontWeight: '700', letterSpacing: '0.5px'}}>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                    </Table.Row>
                    <Table.Row css={{fontSize: '12px', fontWeight: '700', letterSpacing: '0.5px'}}>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                        <Table.Cell><div className="h-8 w-full rounded-md bg-gray-300 animate-pulse"></div></Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        </>
    );
};