"use client"

import './globals.css'
import './globa.scss'

import { Inter } from 'next/font/google'
import {usePathname} from "next/navigation";
import { CssBaseline } from "@nextui-org/react";
import Head from "next/head";
import {SSRProvider} from "@react-aria/ssr";
import {useEffect, useState} from "react";


const inter = Inter({ subsets: ['latin'] })


/*function Loading() {
    const router = useRouter();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const handleStart = (url) => (url !== router.asPath) && setLoading(true);
        const handleComplete = (url) => (url === router.asPath) && setTimeout(() =>{setLoading(false)}, 5000);

        router.events.on('routeChangeStart', handleStart)
        router.events.on('routeChangeComplete', handleComplete)
        router.events.on('routeChangeError', handleComplete)

        return () => {
            router.events.off('routeChangeStart', handleStart)
            router.events.off('routeChangeComplete', handleComplete)
            router.events.off('routeChangeError', handleComplete)
        }

    })

    return loading && (
        <div className={"spinner-wrapper"}>
            <div className={"spinner"}/>
        </div>
    )
}*/

export default function RootLayout({children}: {
  children: React.ReactNode
}) {

    const [token, setToken] = useState('');


    useEffect(() => {
        if (typeof window !== 'undefined') {
            const token = localStorage.getItem('token');
            // @ts-ignore
            setToken(token);
        }
    }, []);

  const pathName = usePathname();

  return (
            <SSRProvider>
                  <html lang="fr">
                      <Head>{CssBaseline.flush()}
                          <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
                      </Head>
                      <body className={inter.className + " bg-[#191a1c]"}>
                        {children}
                      </body>
                  </html>
            </SSRProvider>
      /*) : (
          <SSRProvider>
              <html lang="fr">
              <Head>{CssBaseline.flush()}</Head>
                  <body className={inter.className + "black overflow-y-hidden"}>
                  {token === "" &&
                      <div className="wrapper">
                          <div className="triangle-wrap">
                              <div className="triangle triangle--main"></div>
                              <div className="triangle triangle--red"></div>
                              <div className="triangle triangle--blue"></div>
                              <div className="triangle__text">Loading</div>
                          </div>
                      </div>
                  }
                  </body>
              </html>
          </SSRProvider>
      )*/
  )
}