import { FC } from 'react'

interface loadingProps {}

const loading: FC<loadingProps> = ({}) => {
    return (
        <div className="wrapper">
            <div className="triangle-wrap">
                <div className="triangle triangle--main"></div>
                <div className="triangle triangle--red"></div>
                <div className="triangle triangle--blue"></div>
                <div className="triangle__text">Loading</div>
            </div>
        </div>
    )
}

export default loading