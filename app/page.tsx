"use client"
import {useEffect, useState} from "react";
import {useRouter} from "next/navigation";

export default function Home() {

    let rolesObject = {};
    let role_not_purchase = "";

    const [user, setUser] = useState({})

    useEffect(()=> {
        const localUser = localStorage.getItem('user')

        if(localUser) {
            setUser(JSON.parse(localUser))
        }
        /*setUser(localUser)*/

    }, [])

    const router = useRouter();

    if (user && user.roles) {
        rolesObject = user.roles;
    }

    const rolesArray = Object.values(rolesObject);

    rolesArray.forEach(role => {
        if(role === "ROLE_NOTPURCHASE")
        {
            role_not_purchase = role
        }
    })

    useEffect(() => {

        console.log(user['token'])
        console.log(user['id'])
        console.log(role_not_purchase)

        if(user.token !== null && user.id !== undefined && role_not_purchase == "")
        {
            router.push("/dashboard");

        }else if(user.token !== null && user.id !== undefined && role_not_purchase == "ROLE_NOTPURCHASE")
        {
            router.push("/payment/stripe/plans")

        } else if(user.token == null && user.id == undefined ) {
            router.push("/security/login");
        }
    }, [user]);

}