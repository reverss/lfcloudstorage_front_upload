"use client"

import {Text, Row, Container, Link,} from '@nextui-org/react';
import FormConnexion from "@/components/Form/FormConnexion";
import RegisterModal from "@/components/Modal/RegisterModal";
import Image from "next/image";
import ScrollAnimationWrapper from "@/components/Animation/ScrollAnimationWrapper";
import {motion} from "framer-motion";
import React from "react";

export default function Login(){

    return (
        <>
            <main className={"flex w-full overflow-hidden"}>
                <section className={"left w-4/6 h-screen bg-[#191a1c]"}>
                    <div className={"background-login-left"}>
                        <div className="glitchh">
                            <Image src="/images/neonCityLeft.jpg" width={2000} height={2000} alt=""/>
                            <div className="glitchh__layers">
                                <div className="glitchh__layer"></div>
                                <div className="glitchh__layer"></div>
                                <div className="glitchh__layer"></div>
                            </div>
                        </div>
                        <div className={"absolute mx-auto pt-14 top-[26%] left-[17%] z-[999]"}>
                            <div className="glitch">
                                <Image width={300} height={300} src={'/images/iconedd.png'} alt={'logo'}></Image>
                                <div className="glitch__layers">
                                    <div className="glitch__layer"></div>
                                    <div className="glitch__layer"></div>
                                    <div className="glitch__layer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className={"flex float-right w-7/12 h-screen items-center bg-[#191a1c]"}>
                    <ScrollAnimationWrapper className={"w-full"}>
                        <Container className={"relative w-4/5 h-3/5"}>
                            <motion.div
                                initial={{ opacity: 0 }}
                                animate={{ opacity: 1 }}
                                transition={{
                                    type: "spring",
                                    duration: 2,
                                    delay: 0,
                                    ease: [0, 0.71, 0.2, 1.01]
                                }}
                            >
                                <Row css={{ width: '90%', alignItems: "center" }} justify="space-between">
                                    <Text className={"text-3xl font-bold tracking-wider m-14 mt-16"} css={{color: 'white'}}>
                                        Connexion
                                    </Text>
                                    <Link css={{color: '$purple500', border: '0px !important' }} href={"/"} className={"text-[11px] font-medium mt-4 linkGlitch after:content-['Mot_de_passe_oublié']"}>Mot de passe oublié ?</Link>
                                </Row>
                            </motion.div>
                            <FormConnexion></FormConnexion>
                            <RegisterModal></RegisterModal>
                        </Container>
                    </ScrollAnimationWrapper>
                </section>
            </main>
        </>
    );
}