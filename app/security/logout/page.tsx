"use client"

import React, {useEffect, useState} from "react";
import {useRouter} from "next/navigation";


export default function Logout() {

    let token = null;
    let user_id = null;

    if (typeof localStorage !== 'undefined') {
        token = localStorage.getItem('token');
        user_id = localStorage.getItem('user_id');
    }

    const user = {
        user_id: user_id,
        token: token
    }


    const [jsonData, setJsonData] = React.useState(null);

    const { push } = useRouter();

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://lfcloudstorageapi.reverss.fr/api/logouoot', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(user),
                });
                const jsonData = await response.json();
                setJsonData(jsonData);

                console.log(jsonData)

                if (response.ok) {
                    if(jsonData['resultat'] === 'success') {

                        if (typeof localStorage !== 'undefined') {
                            localStorage.removeItem('token');
                            localStorage.removeItem('user_id');
                            localStorage.removeItem('user')
                        }
                        // Avec message de succès sur le login

                        push('/security/login')

                    }else if(jsonData['resultat'] === 'error_token') {

                        push('/security/login')

                        if (typeof localStorage !== 'undefined') {
                            localStorage.removeItem('token');
                            localStorage.removeItem('user_id');
                            localStorage.removeItem('user')
                        }
                        // Avec message d'erreur sur le login

                    }
                    /*else if(jsonData['resultat'] === 'error_user') {
                        push('/security/login')
                    }*/
                }
            } catch (error) {
            }
        }

        fetchData();
    }, [jsonData, push, user]);


}