"use client"


import {useEffect} from "react";
import axios from "axios";
import {useRouter} from "next/navigation";

export default function SuccessPricingPage() {

    let user_id
    let user;

    if (typeof localStorage !== 'undefined') {
        user = JSON.parse(localStorage.getItem('user'))
    }

    if(user){
        user_id = user['id']
    }
    console.log(user)

    const {push} = useRouter()

    useEffect(() => {

        const fetchData = async () => {
            try {
                const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/role/remove/role_purchase', user_id, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (response.status === 200) {
                    const resultat = response.data['resultat'];
                    if(resultat === 'success')
                    {
                        console.log(response.data)
                       if (typeof localStorage !== 'undefined') {

                            localStorage.removeItem('user')
                            localStorage.setItem('user', JSON.stringify(response.data['user']))
                        }

                        push('/')
                    }
                    console.log('Réponse JSON reçue depuis Symfony :', response.data);
                } else {
                    console.error('Erreur lors de la soumission du formulaire à Symfony');
                }
            } catch (error) {
                console.error('Erreur lors de la soumission du formulaire à Symfony', error);
            }
        }
        fetchData()
    })
}