"use client"

import React, {useState} from "react";
import { loadStripe } from '@stripe/stripe-js';
import axios from "axios";
import {StripeIcon} from "@/components/Icones/StripeIcon";


export default function PricingPage() {

    const [jsonData, setJsonData] = useState(null);

    const handleSubmit = async (event) => {

        event.preventDefault();

        try {
            const response = await fetch('https://lfcloudstorageapi.reverss.fr/order/create-session-stripe', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (response.ok) {
                // La requête a réussi
                const jsonData = await response.json();
                setJsonData(jsonData);
                window.location.href = jsonData.redirect_url;

                console.log(jsonData);
                console.log('Réponse JSON reçu depuis Symfony :', jsonData);
            } else if (response.status === 401) {

                // Gérer les erreurs de la requête
                console.error('Erreur lors de la soumission du formulaire à Symfony');
            }
        } catch (error) {
            // Gérer les erreurs de la requête
            console.error('Erreur lors de la soumission du formulaire à Symfony', error);
        }
    }

    return (
        <main>
            {/*<div id="alert-2" className="w-auto fixed z-[10000] bottom-5 right-5 p-5 text-white alert bg-red-600/80 rounded-xl shadow-lg">
                <div className="flex items-center space-x-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="stroke-current flex-shrink-0 w-6 h-6"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                    <span className="text-sm font-medium tracking-[1px]">Une erreur est survenue, veuillez réessayer</span>
                    <button data-dismiss-target="#alert-2" aria-label="Close" className="p-1 rounded hover:bg-red-400 duration-300">
                        <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </button>
                </div>
            </div>*/}
            <div className="flex antialiased w-full h-screen bg-black text-gray-400 p-10">
                <div className="container px-4 mx-auto my-auto">
                    <div>
                        <div id="title" className="text-center my-10 space-y-2">
                            <h1 className="font-bold text-4xl text-white">Plan tarifaire</h1>
                            <p className="text-light text-gray-500 text-xl">
                                Achat obligatoire pour pouvoir utiliser notre plateforme.
                            </p>
                        </div>
                        <div className="grid grid-cols-1 justify-center pt-10">
                            <div
                                id="plan"
                                className="rounded-xl text-center overflow-hidden w-2/6 mx-auto border-2 border-[#333] transform hover:shadow-2xl hover:scale-[1.02] transition duration-200 ease-in"
                            >
                                <div id="title" className="w-full py-5 border-b border-gray-800">
                                    <h2 className="font-bold text-3xl text-white">Standard</h2>
                                    <h3 className="font-normal text-indigo-500 text-xl mt-2">
                                        €19<sup>,99</sup>
                                    </h3>
                                </div>
                                <div id="content" className="">
                                    <div id="icon" className="my-5">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-12 w-12 mx-auto fill-stroke text-indigo-600"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                        >
                                            <path
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="1"
                                                d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z"
                                            />
                                        </svg>
                                        <p className="text-gray-500 text-sm pt-2">
                                            Parfait pour un stockage cloud privé.
                                        </p>
                                    </div>
                                    <div id="contain" className="w-full leading-8 mb-10 text-lg font-light">
                                        <ul role="list" className="w-3/5 flex flex-col space-y-5 my-7 mx-auto">
                                            <li className="flex space-x-3">
                                                <svg aria-hidden="true" className="flex-shrink-0 w-5 h-5 text-blue-600 dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                                                <span className="text-base font-normal leading-tight text-gray-500 dark:text-gray-400">Transfert à plus de 200Mbit/s</span>
                                            </li>
                                            <li className="flex space-x-3">
                                                <svg aria-hidden="true" className="flex-shrink-0 w-5 h-5 text-blue-600 dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                                                <span className="text-base font-normal leading-tight text-gray-500 dark:text-gray-400">20GB Stockage cloud</span>
                                            </li>
                                            <li className="flex space-x-3">
                                                <svg aria-hidden="true" className="flex-shrink-0 w-5 h-5 text-blue-600 dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                                                <span className="text-base font-normal leading-tight text-gray-500 dark:text-gray-400">Support téléphonique 24/7</span>
                                            </li>
                                            <li className="flex space-x-3">
                                                <svg aria-hidden="true" className="flex-shrink-0 w-5 h-5 text-blue-600 dark:text-blue-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Check icon</title><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                                                <span className="text-base font-normal leading-tight text-gray-500 dark:text-gray-400">Facile à utiliser</span>
                                            </li>
                                        </ul>
                                        <div id="choose" className="w-full mt-10 px-6">
                                            <button
                                                    onClick={handleSubmit}
                                                    type="submit"
                                                    className="w-full flex justify-center bg-gray-900 items-center font-semibold text-lg rounded-xl hover:shadow-lg transition duration-200 ease-in-out hover:bg-white hover:text-black">
                                                Acheter avec <StripeIcon className={"ml-1"} size={50}/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}
