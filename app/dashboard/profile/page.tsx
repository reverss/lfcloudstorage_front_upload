"use client"

import {Button, Input, Link, Text, User,} from "@nextui-org/react";
import {motion} from 'framer-motion'
import React, {useEffect, useState} from "react";
import LinkComponent from "@/components/Link/LinkComponent";
import UploadProfilePictureModal from "@/components/Modal/UploadProfilePictureModal";
import {Mail} from "@/components/Icones/Mail";
import {Label} from "reactstrap";
import {usePathname} from "next/navigation";
import FormProfile from "@/components/Form/FormProfile";

export default function Page() {

    const [user, setUser] = useState({})
    const pathname = usePathname();

    useEffect(()=> {
        const localUser = localStorage.getItem('user')

        if(localUser) {
            setUser(JSON.parse(localUser))
        }

    }, [])

    console.log(user)

    return (
        <>
            <main className={'h-[75%] mx-12'}>
                <motion.div
                    className={"w-4/5 h-full mx-auto my-10"}
                    initial={{opacity: 0, y: 100}}
                    animate={{opacity: 1, y: 0}}
                    transition={{
                        type: "spring",
                        duration: 2,
                        delay: 0.2,
                        ease: [0, 0.71, 0.2, 1.01]
                    }}
                >
                    <section className={'flex items-center justify-between'}>
                        <div>
                            <Text color={"white"} className={"text-2xl tracking-wider"}>Compte</Text>
                            <Text color={"white"} className={"text-sm tracking-wider opacity-60"}>Configurez votre compte ci-dessous.</Text>
                        </div>
                        <div>
                            <Button css={{fontSize: '13px'}} color={'error'} className={"tracking-wider"}>Supprimer son compte</Button>
                        </div>
                    </section>
                    <motion.div
                        className={"flex w-full h-[52px] mx-auto mt-5 rounded-xl bg-gray-100/5 shadow-md"}
                        initial={{opacity: 0, y: 100}}
                        animate={{opacity: 1, y: 0}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            delay: 0.4,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Button.Group css={{marginLeft: 'auto', marginRight: 'auto'}} color="secondary" className={"space-x-4"} flat light>
                            <Link className={pathname === '/dashboard/profile' ? ('activeLinkProfile font-semibold text-sm') : ('')} href={"/dashboard/profile"} css={{transition: 'all 0.25s', borderRadius: '10px !important', width: 'fit-content', padding: '10px 30px 10px 30px', background: 'transparent', color: 'white', '&:hoverq': {background: '$purple200', color: '$purple700'}}}>
                                Profil
                            </Link>
                            <Link className={pathname === '/dashboard/profile/my-security' ? ('activeLinkProfile font-semibold text-sm') : ('font-medium text-sm')} href={"/dashboard/profile/my-security"} css={{transition: 'all 0.25s', borderRadius: '10px !important', width: 'fit-content', padding: '10px 30px 10px 30px', background: 'transparent', color: 'white', '&:hover': {background: '$purple200', color: '$purple700', fontWeight: '600'}}}>
                                Sécurité
                            </Link>
                            <Link className={pathname === '/dashboard/profile/my-security' ? ('activeLinkProfile font-semibold text-sm') : ('font-medium text-sm')} href={"/dashboard/profile/my-stockage"} css={{transition: 'all 0.25s', borderRadius: '10px !important', width: 'fit-content', padding: '10px 30px 10px 30px', background: 'transparent', color: 'white', '&:hover': {background: '$purple200', color: '$purple700', fontWeight: '600'}}}>
                                Stockage
                            </Link>
                        </Button.Group>
                    </motion.div>
                    <FormProfile/>
                </motion.div>
            </main>
        </>
    )
}