"use client"

import { Inter } from 'next/font/google'
import DashboardNavbar from "@/components/Navbar/DashboardNavbar";
import {useSelector} from "react-redux";
import Login from "@/app/security/login/page";
import {useEffect, useState} from "react";
import {any} from "zod";
import {usePathname, useRouter} from "next/navigation";
import {motion} from "framer-motion";
import UploadFileModal from "@/components/Modal/UploadFileModal";
import NavbarTop from "@/components/Navbar/NavbarTop";


const inter = Inter({ subsets: ['latin'] })


export default function RootLayout({children}: {
    children: React.ReactNode
}) {

    let user_id = any;
    let token = any;

    if (typeof localStorage !== 'undefined') {
        token = localStorage.getItem('token');
        user_id = localStorage.getItem('user_id');
    }

    const router = useRouter();
    const pathName = usePathname();

    useEffect(() => {
        if(token === null && user_id === null)
        {
            router.push("/security/login");
        }
    }, [router, token, user_id]);

    return (
        <main className={'flex'}>
            <section className={"w-[19%] h-32"}>
                <DashboardNavbar></DashboardNavbar>
            </section>
            <section className={'w-[81%] h-screen'}>
                {pathName === "/dashboard/files/show" ? (
                    <></>
                ) : (
                    <NavbarTop className={"justify-end"}/>
                )}
                {children}
            </section>
        </main>
    )
}
