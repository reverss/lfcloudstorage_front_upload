"use client"

import {Button, Table, User} from "@nextui-org/react";
import dynamic from "next/dynamic";
import {motion} from "framer-motion";
import NavbarTop from "@/components/Navbar/NavbarTop";
import UploadFileModal from "@/components/Modal/UploadFileModal";
import axios from "axios";
import React, {useEffect, useState} from "react";
import {SkeletonCard} from "@/components/Animation/SkeletonAnimation";
import {formatDate, truncateText} from "@/utils/Functions/globalFunctions";
import {File} from "buffer";
import {TrashIcon} from "@/components/Icones/Dashboard/TrashIcon";


async function getAllFiles() {

    const user_id = localStorage.getItem('user_id');

    let filesList = []

    try {
        const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/my-storage-space/files', user_id, {
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.status === 200) {
            if (response.data.resultat === 'success') {
                filesList = response.data.files;
            }
            console.log('Réponse JSON reçue depuis Symfony :', response.data);
        } else {
            console.error('Erreur lors de la soumission du formulaire à Symfony');
        }
    } catch (error) {
        console.error('Erreur lors de la soumission du formulaire à Symfony', error);
    }
    return filesList
}

function ShowFiles() {

    const [files, setFiles] = useState<Array<File>>([])

    const [loading, setLoading] = useState(true)

    const [selectedKey, setSelectedKey] = useState([])

    useEffect(() => {

        if (files) {
            setTimeout(() => {
                setLoading(false)
            }, 2000);
        }

        if(files.length === 0)
        {
            const fetchData = async () => {
                try {
                    const fetchedFiles = await getAllFiles()
                    setFiles(fetchedFiles)
                } catch (error) {
                    console.error('Erreur lors de la récupération des fichiers:', error)
                }
            }

            fetchData()
        }
    }, [])

    /*const removeFile = (id:number)=> {
        const test = files.filter((file) => {
            return file.id != id
        })
        setFiles(test)
    }*/

    async function removeSelectedFiles() {

        let newSelectedId:any = []

        selectedKey.forEach((key, value) => {
            newSelectedId.push(value)
        })

        // @ts-ignore
        const filesRemaining = files.filter((file) => !newSelectedId.includes(file.id.toString()))
        // @ts-ignore
        const filesToTrash = files.filter((file) => newSelectedId.includes(file.id.toString()))

        setFiles(filesRemaining)

        try {
            const response = await axios.post('https://lfcloudstorageapi.reverss.fr/api/my-storage-space/files/delete', filesToTrash, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.status === 200) {
                if (response.data.resultat === 'success') {
                }
                console.log('Réponse JSON reçue depuis Symfony :', response.data);
            } else {
                console.error('Erreur lors de la soumission du formulaire à Symfony');
            }
        } catch (error) {
            console.error('Erreur lors de la soumission du formulaire à Symfony', error);
        }

        setSelectedKey([])
    }

    return (
        <>
        <NavbarTop className={"justify-between"}>
            <motion.div
                className={"flex items-center"}
                initial={{opacity: 0, x: -100}}
                animate={{opacity: 1, x: 20}}
                transition={{
                    type: "spring",
                    duration: 2,
                    ease: [0, 0.71, 0.2, 1.01]
                }}
            >
                <UploadFileModal/>
                {selectedKey["size"] !== 0 && selectedKey.length !== 0 ? (
                    <motion.div
                        className={"ml-10"}
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        transition={{
                            type: "spring",
                            duration: 2,
                            ease: [0, 0.71, 0.2, 1.01]
                        }}
                    >
                        <Button css={{width: '40%', letterSpacing: '1px', fontSize: '11px'}}
                                className={"flex items-center font-bold hover:opacity-85"} color={"error"}
                                onPress={removeSelectedFiles}>SUPPRIMER <TrashIcon width={15} height={15}/></Button>
                    </motion.div>
                ) : (
                    <>
                    </>
                )}
            </motion.div>
        </NavbarTop>
        <motion.div
            className={"w-11/12 mx-auto mt-6"}
            initial={{opacity: 0, y: 100}}
            animate={{opacity: 1, y: 0}}
            transition={{
                type: "spring",
                duration: 2,
                delay: 0.3,
                ease: [0, 0.71, 0.2, 1.01]
            }}
        >
            {loading ? (
                <SkeletonCard/>
            ) : (
                <>
                    <Table id="table" className={'w-4/5 bg-white shadow-md'}
                           aria-label="Example disabled keys collection table"
                           selectedKeys={selectedKey}
                           onSelectionChange={(selectedKey) => setSelectedKey(selectedKey)}
                           css={{
                               height: "auto",
                               minWidth: "100%",
                           }}
                           selectionMode="multiple"
                           color="secondary"
                    >
                        <Table.Header>
                            <Table.Column css={{paddingLeft: '16px'}}>LOGO</Table.Column>
                            <Table.Column>NOM DE FICHIER</Table.Column>
                            <Table.Column>TAILLE</Table.Column>
                            <Table.Column>DATE UPLOAD</Table.Column>
                            <Table.Column>EXTENSION</Table.Column>
                            {/*<Table.Column><UtilsIcon stroke={'#7E868C'}/></Table.Column>*/}
                        </Table.Header>
                        <Table.Body>
                            {files.map((file, index) => (
                                <Table.Row css={{fontSize: '12px', fontWeight: '700', letterSpacing: '0.5px'}}
                                           key={file.id}>
                                    <Table.Cell><User squared name={""} src={file.url}/></Table.Cell>
                                    <Table.Cell>{truncateText(file.name)}</Table.Cell>
                                    <Table.Cell>{file.size_show}</Table.Cell>
                                    <Table.Cell>{formatDate(file.date_upload.date)}</Table.Cell>
                                    <Table.Cell><span
                                        className={"text-[11px] uppercase"}>{file.type}</span></Table.Cell>
                                </Table.Row>
                            ))}
                        </Table.Body>
                    </Table>
                </>
            )}
        </motion.div>
    </>
)
}


export default dynamic(()=>Promise.resolve(ShowFiles), {ssr: false})